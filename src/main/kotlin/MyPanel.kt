import java.awt.Color
import java.awt.Graphics
import java.awt.Image
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.io.File
import javax.imageio.ImageIO
import javax.swing.JPanel
import javax.swing.Timer

//var playerX = 100
var moveLeft = false
var moveRight = false


class MyPanel: JPanel(){

    var playerX = 100
    val playerY = 680
    val playerWidth = 100
    val playerHeight = 35
    var drops = mutableListOf<Drop>()

    init {
        background = Color.BLACK
        isFocusable = true

        addKeyListener(MyKeyListener())


        for (i in 1..10) drops.add(

            Drop(
                (10..440).random(),
                0,
                ImageIO.read(File("imgs/deda.jpg")),
                80,
                120
            )
        )



        val timer = Timer(16, ActionListener {
            if (moveRight) playerX += 5
            if (moveLeft) playerX -= 5
            for (i in drops) {
                i.y+=2
            }
            repaint()
        }); timer.start()

    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        val gr = g!!

        gr.color = Color.WHITE

        for (i in drops) gr.drawImage(i.image, i.x, i.y, i.width, i.height, null)

        gr.fillRect(playerX, playerY, playerWidth, playerHeight)


    }
}

class MyKeyListener (): KeyListener {

    override fun keyPressed(p0: KeyEvent?) {
        when (p0!!.getKeyCode()){
            KeyEvent.VK_A -> moveLeft = true
            KeyEvent.VK_D -> moveRight = true
            else -> print("")
        }
    }

    override fun keyTyped(p0: KeyEvent?) {

    }

    override fun keyReleased(p0: KeyEvent?) {
        when (p0!!.getKeyCode()){
            KeyEvent.VK_A -> moveLeft = false
            KeyEvent.VK_D -> moveRight = false
            else -> print("")
        }
    }

}

class Drop(
    val x: Int,
    var y: Int,
    val image: Image,
    val width: Int,
    val height: Int
)