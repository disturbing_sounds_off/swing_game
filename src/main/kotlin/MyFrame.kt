import javax.swing.JFrame

class MyFrame: JFrame(){
    init {
        setBounds(450,100,1000,800)
        title = "Swing game with kotlin"
        add(MyPanel())
        isVisible = true
        defaultCloseOperation = EXIT_ON_CLOSE
    }
}